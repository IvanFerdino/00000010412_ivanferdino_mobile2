import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, ToastController } from 'ionic-angular';
import { NoteInterface } from '../../data/note.interface';
import { NoteService } from '../../services/noteService';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  private UserName: string = "";
  private noteListCount: number;
  private notes: NoteInterface[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private noteSvc: NoteService, public modalCtrl: ModalController, private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.presentPrompt();
    this.noteListCount = this.noteSvc.getNoteListLength();
  }


  addNote() {
      let profileModal = this.modalCtrl.create("AddNotePage");
      profileModal.present();
      profileModal.onDidDismiss(data => {
        if(this.noteSvc.addNoteToList(data)){
          console.log("success");
        }
        this.notes = this.noteSvc.getNoteList();
        this.noteListCount = this.noteSvc.getNoteListLength();
        console.log(this.notes);
      });
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Please Write Your Name',
      enableBackdropDismiss : false,
      inputs: [
        {
          name: 'username',
          placeholder: 'Username'
        }
      ],
      buttons: [
        {
          text: 'OK',
          handler: data => {
            // console.log(data.username);
            this.UserName = data.username;
            this.noteSvc.setCurrentUser(this.UserName);
          }
        },
      ]
    });
    alert.present();
  }

  viewNote(index: number) {
    // console.log(index);
    this.navCtrl.push("DetailsPage",this.noteSvc.getSingleNote(index));
  }

  deleteNote(index: number) {
    console.log(index);
    this.noteSvc.removeNoteFromListByIndex(index);

    this.notes = this.noteSvc.getNoteList();
    this.noteListCount = this.noteSvc.getNoteListLength();

    let toast = this.toastCtrl.create({
      message: 'Note Has Been Deleted!',
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
     
    });
  
    toast.present();
  }
}

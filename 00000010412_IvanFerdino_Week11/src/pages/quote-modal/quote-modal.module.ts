import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuoteModalPage } from './quote-modal';

@NgModule({
  declarations: [
    QuoteModalPage,
  ],
  imports: [
    IonicPageModule.forChild(QuoteModalPage),
  ],
})
export class QuoteModalPageModule {}

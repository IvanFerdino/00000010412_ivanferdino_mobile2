export interface Data { //format dari Data
    nama: string, 
    nim: string, 
    judul: string, 
    classOf: string, 
    dospem: string[], 
    lintasProdi: boolean
}
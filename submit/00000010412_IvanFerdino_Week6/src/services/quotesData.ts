import { Injectable } from '@angular/core';
import { Quote } from '../data/quote.interface';
import quotes from '../data/quotes'; //quotes.ts file//quote interface


@Injectable()
export class QuotesData {
    private insertObj: Quote;
    private countId = 13;//start dr 13 soalnya
  public quotesCollection: { category: string, quotes: Quote[], icon: string } []; 
  
  initialize() {
      this.quotesCollection = quotes;
  }

  getData() {
      return this.quotesCollection;
  }

  getCount() {
      return this.countId;
  }

  addNewQuoteByCategory(cat: string, text: string, author: string) {
    var index = this.quotesCollection.map(function(o) { return o.category; }).indexOf(cat);//dpt in index dr cat tsb

    //masuk ke index cat tsb, masuk ke array quote nya, baru di push
    this.insertObj = {id: this.countId++, person: author, text: text};
    this.quotesCollection[index].quotes.push(this.insertObj);//insert langsung by obj

    //insert manual buat obj dr sini langsung
    // this.quotesCollection[index].quotes.push({
    //     id: this.countId++, 
    //     person: author, 
    //     text
    // });



    // console.log(index);

    // this.quotesCollection.indexOf(category);
    
  }

}
import { Data } from '../data/data.interface'; //quote interface

export class DataSkripsi {
    private data: Data[] = []; // arrau of Data
    setData(data: Data){
        // console.log(data);
        this.data.push(data); //insert ke array
        return true;
    }

    getData() {
        return this.data; //return array of data
    }

    getLength() {
        return this.data.length;
    }
}
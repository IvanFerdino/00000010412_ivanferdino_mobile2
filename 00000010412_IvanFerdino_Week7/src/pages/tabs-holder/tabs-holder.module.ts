import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabsHolderPage } from './tabs-holder';

@NgModule({
  declarations: [
    TabsHolderPage,
  ],
  imports: [
    IonicPageModule.forChild(TabsHolderPage),
  ],
})
export class TabsHolderPageModule {}

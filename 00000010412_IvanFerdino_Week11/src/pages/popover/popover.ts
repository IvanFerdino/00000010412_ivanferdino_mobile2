import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';

/**
 * Generated class for the PopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popover',
  template: `
  <ion-list>
      <ion-list-header>Ionic</ion-list-header>
      <button ion-item (click)="showAbout()">About Me!</button>
    </ion-list>
  `,
})
export class PopoverPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverPage');
  }
  showAbout() {
    this.viewCtrl.dismiss();
    this.presentToast();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Ivan Ferdino - 000 000 10412',
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
}

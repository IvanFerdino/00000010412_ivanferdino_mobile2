// import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';
import { Quote } from '../data/quote.interface';
import { AuthService } from './authService';
// import { Http } from '@angular/http'; 
import { HttpClient } from '@angular/common/http';


@Injectable()
export class FavoriteListServices {
    public favoriteList: Quote[] = [];
//   public loginState:boolean = false;
constructor(private authSvc: AuthService, private http:HttpClient){

}
reset() {
    this.favoriteList = [];
}
    addQuoteToFav(quote: Quote){
        // console.log(quote);
        // console.log("asdsa");
        this.favoriteList.push(quote);
        this.authSvc.getCurrentUser().getIdToken().then(
            (token: string)=>{
              console.log(token);
                const userID = this.authSvc.getCurrentUser().uid;
                this.http.put("https://mobile2mingguan.firebaseio.com/"+userID+"/data.json?auth="+token, this.favoriteList).subscribe(
                    ()=> console.log("success"),error=>{
                        console.log("error");
                    }
                );
            });
    }
    
    


    removeQuoteFromFav(quote: Quote){
        // console.log("removed");
        console.log("removed", quote);
        // this.favoriteList.delete(quote);
        this.favoriteList.splice( this.favoriteList.indexOf(quote), 1 );

    }
    isFavorite(quote: Quote){
        if(this.favoriteList.indexOf(quote) == -1){
            //not found
            // console.log("not found");
            return "false";

        }else{
            //found
            // console.log("found");
            return "true";
        }
    }
    getFavList(){
        // console.log("get list");
        // console.log(this.favoriteList);
        return this.favoriteList;
    }
    getFavListCount() {
        return this.favoriteList.length;
    }

}
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Loc } from '../../data/loc.class';


/**
 * Generated class for the SetLocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-set-location',
  templateUrl: 'set-location.html',
})
export class SetLocationPage {
  // marker= {lat: 0, lng: 0};
  marker: Loc;
  lat:number = -6.178306; //lat lng default
  lng:number = 106.631889;
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController) {
    this.marker = new Loc(this.lat,this.lng); //INI PAKE CLASS, TP DI BLG PAKE INTERFACE
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetLocationPage');
  }
  onClose() {
    this.viewCtrl.dismiss();
  }
  onConfirm(){ // balikin data coordinat yg dipilih
    //set the latitude & longitude selected on the map and return it back to the AddPlacePage
    let data = {lat: this.lat, lng: this.lng}
    this.viewCtrl.dismiss(data);
  }

  onSetMarker(event: any) { // set ulang lat lng, sama set marker sesuai yg dipilih
    console.log(event);
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;
    this.marker.setLatLng(event.coords.lat,event.coords.lng);//set ulang markernya sesuai yg di pilih lokasinya
  }


}

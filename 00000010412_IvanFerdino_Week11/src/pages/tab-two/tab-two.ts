import { Component, OnChanges, AfterContentInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, ToastController } from 'ionic-angular';
import { Quote } from '../../data/quote.interface';
import { FavoriteListServices } from '../../services/favoriteList';
import { QuoteModalPage } from '../quote-modal/quote-modal';
import { SettingsService } from '../../services/settings';


/**
 * Generated class for the TabTwoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-two',
  templateUrl: 'tab-two.html',
})
export class TabTwoPage {
  quotes: Quote[];
  quote: Quote;
  size: number;
  constructor(public navCtrl: NavController, public navParams: NavParams, public favList: FavoriteListServices, public alertCtrl: AlertController, public modalCtrl: ModalController, public settingsSvc: SettingsService, private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    // console.log(this.favList);

    console.log('ionViewDidLoad TabTwoPage');
  }

  ionViewDidEnter() {
    this.quotes = this.favList.getFavList();
    this.size = this.quotes.length;
    console.log(this.quotes);
    // this.favList.getFavList();

  }

  addNewQuotes() {
    let alert = this.alertCtrl.create({
      title: 'Add New Quote To List',
      inputs: [
        {
          name: 'author',
          placeholder: 'Author'
        },
        {
          name: 'quote',
          placeholder: 'Quote'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add',
          handler: data => {
            this.quote = {id: 0, person: data.author, text: data.quote};
            // this.insertQuote = {id: 0, person: "adsd", text: "text"}
            this.favList.addQuoteToFav(this.quote);
            this.presentToast();
          }
        }
      ]
    });
    alert.present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Quote was added successfully',
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  quoteViewModal(singleQuoteData: Quote) {
    console.log(singleQuoteData);
    let quoteModal = this.modalCtrl.create(QuoteModalPage, {'quote': singleQuoteData}, {showBackdrop: true, enableBackdropDismiss: true});
    quoteModal.present();
    quoteModal.onDidDismiss((param: any)=> {
      console.log(param);
    });
  }

  setBgColor() {
    // console.log(this.settingsSvc.currentState() ? 'altQuoteBg' : '');
    return this.settingsSvc.currentState() ? 'altQuoteBg' : '';
  }


  removeFromFavorite(singleQuoteData: Quote) {
    // console.log("a");
      const confirm = this.alertCtrl.create({
        title: 'Unfavorite This Quote?',
        message: 'Are you sure you want to remove this quote from favorite?',
        buttons: [
          {
            text: 'Disagree',
            handler: () => {
              // console.log('Disagree clicked');
            }
          },
          {
            text: 'Agree',
            handler: () => {
              // console.log('Agree clicked');
              // console.log(singleQuoteData);
              // this.favList.favoriteList.push(singleQuoteData);//insert ke service favList
              this.favList.removeQuoteFromFav(singleQuoteData);//insert ke service favList
            }
          }
        ]
      });
      confirm.present();
    }

}

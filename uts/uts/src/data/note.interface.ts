export interface NoteInterface {
    title: string,
    author: string,
    text: string
}
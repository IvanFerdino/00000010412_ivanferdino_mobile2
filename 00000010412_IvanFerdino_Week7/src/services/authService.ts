import firebase from 'firebase';
import { ToastController } from 'ionic-angular';
import { Injectable } from '@angular/core';


@Injectable()
export class AuthService{
    constructor(private toastCtrl: ToastController){}
    signup(email: string, password: string) {
        return firebase.auth().createUserWithEmailAndPassword(email, password);
    }

    signin(email: string, password: string) {
        return firebase.auth().signInWithEmailAndPassword(email, password);
    }

    logout() {
        if(firebase.auth().signOut()){
            // this.favList.reset();
            return true;

        }else{
            return false;
        }
    }

    getCurrentUser() {
        return firebase.auth().currentUser;
        //this function rturn object, use ".uid" to get user id of current user
    }

    getCurrentUserToken() {
        return firebase.auth().currentUser.getIdToken();
    }

    presentToast(message: any, status: boolean) {
        let toast;
        console.log(message);
        console.log(status);
        if(status){
            toast = this.toastCtrl.create({
                message: message+' was successful',
                duration: 3000,
                position: 'bottom'
                });
        }else{
            toast = this.toastCtrl.create({
                message: message,
                duration: 3000,
                position: 'bottom'
                });
        }
        
        
        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });
      
        toast.present();
      }
    
}
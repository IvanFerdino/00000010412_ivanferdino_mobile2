import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Alert, AlertController } from 'ionic-angular';
import { Quote } from '@angular/compiler';
import { FavoriteListServices } from '../../services/favoriteList';

/**
 * Generated class for the QuoteModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quote-modal',
  templateUrl: 'quote-modal.html',
})
export class QuoteModalPage {
  quote: Quote;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public favList: FavoriteListServices, public alertCtrl: AlertController) {
    this.quote = this.navParams.get('quote');
    //this.navParams,data;
    // console.log(this.quote);
  }
  closeModal() {
    this.viewCtrl.dismiss("modal closed"); 
  }

  removeFromFavorite(singleQuoteData: Quote) {
    // console.log("a");
      const confirm = this.alertCtrl.create({
        title: 'Unfavorite This Quote?',
        message: 'Are you sure you want to remove this quote from favorite?',
        buttons: [
          {
            text: 'Disagree',
            handler: () => {
              // console.log('Disagree clicked');
            }
          },
          {
            text: 'Agree',
            handler: () => {
              // console.log('Agree clicked');
              // console.log(singleQuoteData);
              // this.favList.favoriteList.push(singleQuoteData);//insert ke service favList
              this.favList.removeQuoteFromFav(singleQuoteData);//insert ke service favList
              this.viewCtrl.dismiss("modal closed and quote removed from list");
            }
          }
        ]
      });
      confirm.present();
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuoteModalPage');
  }

}

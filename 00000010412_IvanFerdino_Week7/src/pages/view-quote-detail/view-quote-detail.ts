import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { Quote } from '../../data/quote.interface';
import { FavoriteListServices } from '../../services/favoriteList';
import { QuotesData } from '../../services/quotesData';


/**
 * Generated class for the ViewQuoteDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-quote-detail',
  templateUrl: 'view-quote-detail.html',
})
export class ViewQuoteDetailPage implements OnInit {
  quoteData: { category: string, quotes: Quote[], icon: string } [];
  
  ngOnInit() {
    this.quoteData = this.navParams.data;
    console.log(this.quoteData);
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public favList: FavoriteListServices, private quoteDataService: QuotesData, private toastCtrl: ToastController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewQuoteDetailPage');
  }

  addNewQuotes() {
    let alert = this.alertCtrl.create({
      title: 'Add New Quote To '+ this.navParams.data.category,
      inputs: [
        {
          name: 'author',
          placeholder: 'Author'
        },
        {
          name: 'quote',
          placeholder: 'Quote'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add',
          handler: data => {
            this.quoteDataService.addNewQuoteByCategory(this.navParams.data.category, data.quote, data.author);
            this.presentToast();
          }
        }
      ]
    });
    alert.present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Quote was added successfully',
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  addToFavorite(singleQuoteData: Quote) {
    // console.log("a");
    if(this.favList.isFavorite(singleQuoteData) == "false"){
        const confirm = this.alertCtrl.create({
          title: 'Favorite This Quote?',
          message: 'Are you sure you want to add this quote to favorite?',
          buttons: [
            {
              text: 'Disagree',
              handler: () => {
                // console.log('Disagree clicked');
              }
            },
            {
              text: 'Agree',
              handler: () => {
                // console.log('Agree clicked');
                // console.log(singleQuoteData);
                // this.favList.favoriteList.push(singleQuoteData);//insert ke service favList
                this.favList.addQuoteToFav(singleQuoteData);//insert ke service favList
              }
            }
          ]
        });
      confirm.present();
    }
    if(this.favList.isFavorite(singleQuoteData) == "true"){
        const confirm = this.alertCtrl.create({
          title: 'This Quote Already in Favorite List!',
          message: "You Can't Add Same Quote Twice!",
          buttons: [
            {
              text: 'Close',
              handler: () => {
                // console.log('Disagree clicked');
              }
            }
          ]
        });
      confirm.present();
    }
  }

}

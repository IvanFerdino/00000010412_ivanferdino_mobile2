export class SettingsService {
    private altBg = false;

    setBg(isAlt: boolean){
        this.altBg = isAlt;
    }

    currentState() {
        return this.altBg;
    }
}
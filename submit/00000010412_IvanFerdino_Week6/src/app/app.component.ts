import { Component, ViewChild } from "@angular/core";
import { Platform, NavController, MenuController } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";

import { HomePage } from "../pages/home/home";
import { TabsHolderPage } from "../pages/tabs-holder/tabs-holder";
import { SettingsPage } from "../pages/settings/settings";
import { QuotesData } from "../services/quotesData";
@Component({
  templateUrl: "app.html"
})
export class MyApp {
  rootPage: any = TabsHolderPage;
  tabsPage = TabsHolderPage;
  settingsPage = SettingsPage;

  @ViewChild("sideMenuContent")
  navCtrl: NavController;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public menuCtrl: MenuController,
    private quoteData: QuotesData
  ) {
    this.quoteData.initialize();
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  onLoad(page: any) {
    this.navCtrl.setRoot(page);
    this.menuCtrl.close();
  }
}

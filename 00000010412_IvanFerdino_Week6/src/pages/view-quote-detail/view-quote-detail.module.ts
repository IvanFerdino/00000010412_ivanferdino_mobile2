import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewQuoteDetailPage } from './view-quote-detail';

@NgModule({
  declarations: [
    ViewQuoteDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewQuoteDetailPage),
  ],
})
export class ViewQuoteDetailPageModule {}

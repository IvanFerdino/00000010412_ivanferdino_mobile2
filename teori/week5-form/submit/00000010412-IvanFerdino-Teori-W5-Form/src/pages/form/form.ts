import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the FormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form',
  templateUrl: 'form.html',
})
export class FormPage {
  // https://ionicthemes.com/tutorials/about/forms-and-validation-in-ionic
  // https://stackoverflow.com/questions/38064592/angular-2-form-validation-minlength-validator-is-not-working
  // https://forum.ionicframework.com/t/a-good-form-validation-example/117960
  private formRegistrasi: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, private toast:ToastController) {
    this.formRegistrasi = this.formBuilder.group({
      username: [
        "", 
        Validators.compose([
          Validators.required, 
          Validators.minLength(1), 
          Validators.maxLength(10)
        ])
      ],
      nim: ["", Validators.required],
      fname: ["", Validators.required],
      lname: [""],
      email: ["", Validators.required],
      password: ["", Validators.required],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormPage');
  }

  SubmitForm() {
    console.log(this.formRegistrasi.value);
    let toast = this.toast.create({
      message: 'Submitted successfully, see your console log!',
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
}

import firebase from 'firebase';

export class AuthService{
    signup(email: string, password: string) {
        return firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log("a",errorCode);
         });
    }

    signin(email: string, password: string) {
        return firebase.auth().signInWithEmailAndPassword(email, password);
    }

    logout() {
        if(firebase.auth().signOut()){
            return true;

        }else{
            return false;
        }
    }
}
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DataSkripsi } from '../../services/dataSkripsi';
import { Data } from '../../data/data.interface';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
  data: Data[] = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public dataList: DataSkripsi) {
  }

  ionViewDidLoad() {
    this.data = this.dataList.getData();
    // console.log(this.data);
    console.log('ionViewDidLoad ModalPage');
  }

  closeModal(){
    this.viewCtrl.dismiss("modal closed");
  }

}

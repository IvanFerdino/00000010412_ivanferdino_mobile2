import { Component, OnChanges, AfterContentInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Quote } from '@angular/compiler';
import { FavoriteListServices } from '../../services/favoriteList';


/**
 * Generated class for the TabTwoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-two',
  templateUrl: 'tab-two.html',
})
export class TabTwoPage {
  quotes: Quote[];
  size: number;
  constructor(public navCtrl: NavController, public navParams: NavParams, public favList: FavoriteListServices, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    // console.log(this.favList);

    console.log('ionViewDidLoad TabTwoPage');
  }

  ionViewDidEnter() {
    this.quotes = this.favList.getFavList();
    this.size = this.quotes.length;
    console.log(this.quotes);
    // this.favList.getFavList();

  }

  removeFromFavorite(singleQuoteData: Quote) {
    // console.log("a");
      const confirm = this.alertCtrl.create({
        title: 'Unfavorite This Quote?',
        message: 'Are you sure you want to remove this quote from favorite?',
        buttons: [
          {
            text: 'Disagree',
            handler: () => {
              // console.log('Disagree clicked');
            }
          },
          {
            text: 'Agree',
            handler: () => {
              // console.log('Agree clicked');
              // console.log(singleQuoteData);
              // this.favList.favoriteList.push(singleQuoteData);//insert ke service favList
              this.favList.removeQuoteFromFav(singleQuoteData);//insert ke service favList
            }
          }
        ]
      });
      confirm.present();
    }

}

import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Quote } from '@angular/compiler';
import { FavoriteListServices } from '../../services/favoriteList';


/**
 * Generated class for the ViewQuoteDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-quote-detail',
  templateUrl: 'view-quote-detail.html',
})
export class ViewQuoteDetailPage implements OnInit {
  quoteData: { category: string, quotes: Quote[], icon: string } [];
  
  ngOnInit() {
    this.quoteData = this.navParams.data;
    // console.log(this.quoteData);
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public favList: FavoriteListServices) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewQuoteDetailPage');
  }

  addToFavorite(singleQuoteData: Quote) {
    // console.log(singleQuoteData.person);
    // console.log("a");
      const confirm = this.alertCtrl.create({
        title: 'Favorite This Quote?',
        message: 'Are you sure you want to add this quote to favorite?',
        buttons: [
          {
            text: 'Disagree',
            handler: () => {
              // console.log('Disagree clicked');
            }
          },
          {
            text: 'Agree',
            handler: () => {
              // console.log('Agree clicked');
              this.favList.favoriteList.push(singleQuoteData);//insert ke service favList
            }
          }
        ]
      });
      confirm.present();
    }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/authService';

/**
 * Generated class for the AuthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
})
export class AuthPage {
  loginResult: {status: boolean, errorData: {code: string, message: string}};
  private signInForm: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, private authService: AuthService, private toastCtrl: ToastController) {
    this.signInForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AuthPage');
  }

  signin() {
    console.log(this.signInForm.value.email);
    this.authService.signin(this.signInForm.value.email, this.signInForm.value.password).then(
      ()=>{
        console.log("success");
        this.authService.presentToast("Sign In", true);
        //retrieve from firebase for this current user, get uid and get token, then insert to local array/service
        
        //yg belom: update pas unfav, pas buka load dr db
      }
    ).catch((err)=>{
        console.log(err);
        this.authService.presentToast(err.message, false);
    });
  }


}

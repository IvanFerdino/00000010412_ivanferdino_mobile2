import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  formPage = 'FormPage';
  constructor(public navCtrl: NavController) {

  }
  pindah() {
    this.navCtrl.push('FormPage');
  }

}

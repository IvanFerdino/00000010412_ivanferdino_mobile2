import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabsHolderPage } from '../pages/tabs-holder/tabs-holder';
import { TabOnePage } from '../pages/tab-one/tab-one';
import { TabTwoPage } from '../pages/tab-two/tab-two';
import { ViewQuoteDetailPage } from '../pages/view-quote-detail/view-quote-detail';
import { QuoteModalPage } from '../pages/quote-modal/quote-modal';
import { SettingsPage } from '../pages/settings/settings';


import { FavoriteListServices } from '../services/favoriteList';
import { SettingsService } from '../services/settings';
import { QuotesData } from '../services/quotesData';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsHolderPage,
    TabOnePage,
    TabTwoPage,
    ViewQuoteDetailPage,
    QuoteModalPage,
    SettingsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsHolderPage,
    TabOnePage,
    TabTwoPage,
    ViewQuoteDetailPage,
    QuoteModalPage,
    SettingsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FavoriteListServices,
    SettingsService,
    QuotesData
  ]
})
export class AppModule {}

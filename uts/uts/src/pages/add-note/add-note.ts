import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '../../../node_modules/@angular/forms';
import { NoteInterface } from '../../data/note.interface';
import { NoteService } from '../../services/noteService';

/**
 * Generated class for the AddNotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-note',
  templateUrl: 'add-note.html',
})
export class AddNotePage {
  singleNote: NoteInterface;
  private formAddNote: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private formBuilder: FormBuilder, private toast:ToastController, private noteSvc: NoteService) {
    this.formAddNote = this.formBuilder.group({
      title: ["", Validators.required],
      author: ["", Validators.required],
      teks: ["", Validators.required]
    });
  }

  SubmitForm() {
    // console.log(this.formAddNote.value);
    this.singleNote = { title: this.formAddNote.value.title, author: this.formAddNote.value.author, text: this.formAddNote.value.teks };
    // console.log(this.singleNote);
    this.close(this.singleNote);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad AddNotePage');
    console.log(this.navParams.data);
  }

  close(data: NoteInterface) {
    this.viewCtrl.dismiss(data);
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabOnePage } from '../tab-one/tab-one';
import { TabTwoPage } from '../tab-two/tab-two';

/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  template: `
  <ion-tabs>
    <ion-tab [root]="tabOne" tabTitle="Library" tabIcon="book"></ion-tab>
    <ion-tab [root]="tabTwo" tabTitle="Favorite" tabIcon="star"></ion-tab>
  </ion-tabs>
  `
})
export class TabsPage {
  tabOne = TabOnePage;
  tabTwo = TabTwoPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}

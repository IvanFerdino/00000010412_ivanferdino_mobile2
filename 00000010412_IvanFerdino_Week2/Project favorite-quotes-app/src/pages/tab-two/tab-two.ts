import { Component, OnChanges, AfterContentInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Quote } from '@angular/compiler';
import { FavoriteListServices } from '../../services/favoriteList';


/**
 * Generated class for the TabTwoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-two',
  templateUrl: 'tab-two.html',
})
export class TabTwoPage {
  quotes: Quote[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public favList: FavoriteListServices) {
  }

  ionViewDidLoad() {
    // console.log(this.favList);

    console.log('ionViewDidLoad TabTwoPage');
  }

  ionViewDidEnter() {
    console.log(this.favList);
    // this.favList.getFavList();

  }
}

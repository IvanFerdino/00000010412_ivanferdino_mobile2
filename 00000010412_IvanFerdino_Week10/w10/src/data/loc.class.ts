export class Loc {
    lat: number;
    lng: number;
    constructor(lat: number, lng:number) {
        this.lat = lat;
        this.lng = lng;
    }
    setLatLng(lat:number, lng:number) {
        this.lat = lat;
        this.lng = lng;
    }
}
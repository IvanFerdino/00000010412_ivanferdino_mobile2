import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Quote } from '../../data/quote.interface'; //quote interface
import { ViewQuoteDetailPage } from '../view-quote-detail/view-quote-detail';
import { FavoriteListServices } from '../../services/favoriteList';
import { QuotesData } from '../../services/quotesData';


@IonicPage()
@Component({
  selector: 'page-tab-one',
  templateUrl: 'tab-one.html',
})
export class TabOnePage implements OnInit {
  viewQuotePage = ViewQuoteDetailPage;
  quotesCollection: { category: string, quotes: Quote[], icon: string } []; 
  // ada array karena ada array of quotes, yg di dlm nya terdiri dari category, quotes, dan icon
  //Quote[] karena di dlm quotes, meimiliki lebih dr 1 Quote

  constructor(public navCtrl: NavController, public navParams: NavParams, public favList: FavoriteListServices, private quoteData: QuotesData) {
  }
  
  ngOnInit(){ //run sekali di awal buat masukin data2 dari quotes.ts
    this.quotesCollection = this.quoteData.getData(); //langsung dr service
    // this.quoteData.addNewQuoteByCategory("life","asdasd","dasdas");
    console.log(this.quotesCollection);
    console.log(this.quoteData.getCount());

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad TabOnePage');
  }

}

import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  ModalController
} from "ionic-angular";
import { FormGroup, FormBuilder, FormArray, Validators } from "@angular/forms";
import { DataSkripsi } from "../../services/dataSkripsi"; // service untuk menampung list of data
import { Data } from "../../data/data.interface"; //data interface

// import { Data } from '../../data/data.interface'; //quote interface

/**
 * Generated class for the FormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-form",
  templateUrl: "form.html"
})
export class FormPage {
  private pendataanSkripsi: FormGroup;
  private dospem: string[] = [];
  private countDospem = 1;//default ada 1
  data: Data;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    public dataList: DataSkripsi,
    public modalCtrl: ModalController
  ) {
    this.pendataanSkripsi = this.formBuilder.group({
      nama: ["", Validators.required],
      nim: ["", Validators.required],
      judul: ["", Validators.required],
      classOf: ["", Validators.required],
      dospem: this.formBuilder.array([this.initDospemField()]),
      lintasProdi: ['']
    });
  }

  /* DOKUMENTASI DYNAMIC FORM
  http://masteringionic.com/blog/2018-02-06-dynamically-add-and-remove-form-input-fields-with-ionic/
  
  */

  initDospemField() {
    return this.formBuilder.group({
      dospem: ["", Validators.required]
    });
  }

  addNewInputField(): void {
    this.countDospem++;
    if(this.countDospem<3){
      const control = <FormArray>this.pendataanSkripsi.controls.dospem;
      control.push(this.initDospemField());
    }else{
      let toast = this.toastCtrl.create({
        message: "Can't Add More Than 2!",
        duration: 1500,
        position: "bottom"
      });

      toast.onDidDismiss(() => {
        // console.log('Dismissed toast');
      });

      toast.present();
    }
  }

  removeInputField(i: number): void {
    this.countDospem = 1;
    const control = <FormArray>this.pendataanSkripsi.controls.dospem;
    control.removeAt(i);
  }

  logForm() {
    // this.dospem.push(this.pendataanSkripsi.value.dospem);
    this.dospem.push(this.pendataanSkripsi.value.dospem.slice(0)); // ini supaya ke clone, kalo ga dia by reference bukan by value!

    //set data ke interface Data
    this.data = {
      nama: this.pendataanSkripsi.value.nama,
      nim: this.pendataanSkripsi.value.nim,
      judul: this.pendataanSkripsi.value.judul,
      classOf: this.pendataanSkripsi.value.classOf,
      dospem: this.dospem.slice(0), //supaya by value bukan by reference
      lintasProdi: this.pendataanSkripsi.value.lintasProdi
    };
  
    // console.log(this.data);

    //insert Data ke Service DataList
    if (this.dataList.setData(this.data)) {
      let toast = this.toastCtrl.create({
        message: "Submit Success!",
        duration: 1500,
        position: "bottom"
      });

      toast.onDidDismiss(() => {
        // console.log('Dismissed toast');
      });

      toast.present();
    } else {
      let toast = this.toastCtrl.create({
        message: "Submit Failed!",
        duration: 1500,
        position: "bottom"
      });

      toast.onDidDismiss(() => {
        // console.log('Dismissed toast');
      });

      toast.present();
    }

    //untuk reset form
    // console.log(this.pendataanSkripsi.value.dospem);
    this.resetDospemFormArray(); //reset form array
    this.pendataanSkripsi.reset(); //reset form
    // console.log(this.pendataanSkripsi.value.dospem);
  }

  resetDospemFormArray() {
    // reset array yg dipakai untuk nampung input
    this.dospem.length = 0;
    this.pendataanSkripsi.value.dospem = [];
    for (var i = 0; i < this.pendataanSkripsi.value.dospem.length; i++) {
      this.pendataanSkripsi.value.dospem[i] = null;
    }
    this.pendataanSkripsi.value.dospem.length = 0;
  }

  showData() {
    //show ke modal
    // console.log(this.dataList.getData());
    let modal = this.modalCtrl.create("ModalPage");
    modal.present();
    modal.onDidDismiss((param1: any) => {
      console.log(param1);
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad FormPage");
  }

  showReference() {
    let url = "http://masteringionic.com/blog/2018-02-06-dynamically-add-and-remove-form-input-fields-with-ionic/";
    let toast = this.toastCtrl.create({
      message: url,
      duration: 3000,
      position: "bottom"
    });

    toast.onDidDismiss(() => {
      // console.log('Dismissed toast');
    });

    toast.present();
  }
}

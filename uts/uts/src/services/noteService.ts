import { NoteInterface } from '../data/note.interface';
export class NoteService {
    private noteList: NoteInterface[] = [];
    private currentUser: string = "";
    constructor(){

    }
    setCurrentUser(user: string) {
        this.currentUser = user;
        console.log("setCurrentUser Success");
        return true;
    }
    getCurrentUser() {
        return this.currentUser;
    }
    addNoteToList(note: NoteInterface){
        this.noteList.push(note);
        return true;
    }
    removeNoteFromList(note: NoteInterface){
        console.log("removed", note);
        this.noteList.splice( this.noteList.indexOf(note), 1 );
        return true;
    }
    removeNoteFromListByIndex(index: number){
        console.log("removed", index);
        this.noteList.splice(index, 1 );
        return true;
    }
    getNoteList(){
        // console.log("get list");
        // console.log(this.favoriteList);
        return this.noteList;
    }
    getSingleNote(index:number){
        return this.noteList[index];
    }
    getNoteListLength() {
        // return 1;
        return this.noteList.length;
    }

}

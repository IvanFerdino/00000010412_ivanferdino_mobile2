import { Injectable } from '@angular/core';
import { Quote } from '@angular/compiler';
@Injectable()
export class FavoriteListServices {
    public favoriteList: Quote[] = [];
//   public loginState:boolean = false;
    addQuoteToFav(quote: Quote){
        // console.log(quote);
        // console.log("asdsa");
        this.favoriteList.push(quote);
    }
    removeQuoteFromFav(quote: Quote){
        // console.log("removed");
        console.log("removed", quote);
        // this.favoriteList.delete(quote);
        this.favoriteList.splice( this.favoriteList.indexOf(quote), 1 );

    }
    isFavorite(quote: Quote){
        if(this.favoriteList.indexOf(quote) == -1){
            //not found
            // console.log("not found");
            return "false";

        }else{
            //found
            // console.log("found");
            return "true";
        }
    }
    getFavList(){
        // console.log("get list");
        // console.log(this.favoriteList);
        return this.favoriteList;
    }

}
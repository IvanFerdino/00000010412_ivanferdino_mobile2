import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthService } from '../../services/AuthService'
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private authService: AuthService) {

  }
  login() {
    if(this.authService.signin("nama@google.com","ivanivan")){
      console.log("loggin success");
    }else{
      console.log("fail");
    }
  }

  logout() {
    // console.log(this.authService.logout());
    if(this.authService.logout()){
      console.log("logout success");
    }else{
      console.log("fail");
    }
  }

  signup() {
    // console.log(this.authService.signup("nama2@gmail.com","passpass"));
    if(this.authService.signup("nama2@gmail.com","passpass")){
      console.log("signup success");
    }else{
      console.log("fail");
    }
  }

}

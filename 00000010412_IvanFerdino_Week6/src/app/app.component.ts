import { Component, ViewChild } from "@angular/core";
import { Platform, NavController, MenuController } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";

import { HomePage } from "../pages/home/home";
import { TabsHolderPage } from "../pages/tabs-holder/tabs-holder";
import { SettingsPage } from "../pages/settings/settings";
import { QuotesData } from "../services/quotesData";

import firebase, { auth } from 'firebase';
import { AuthPage } from "../pages/auth/auth";
import { AuthService } from "../services/authService";
import { SignupPage } from '../pages/signup/signup';
import { FavoriteListServices } from "../services/favoriteList";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  rootPage: any = AuthPage;
  tabsPage = TabsHolderPage;
  settingsPage = SettingsPage;

  signInPage = AuthPage;
  signUpPage = SignupPage;
  private login: boolean;

  @ViewChild("sideMenuContent")
  navCtrl: NavController;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public menuCtrl: MenuController,
    private quoteData: QuotesData,
    private authService: AuthService,
    private favList: FavoriteListServices
  ) {
    firebase.initializeApp({
      apiKey: "AIzaSyAAyq4zeKuolAAbEGANqyMHUHnlJMQEMMY",
      authDomain: "mobile2mingguan.firebaseapp.com",
      databaseURL: "https://mobile2mingguan.firebaseio.com",
      projectId: "mobile2mingguan",
      storageBucket: "mobile2mingguan.appspot.com",
      messagingSenderId: "825852779761"
    });
    firebase.auth().onAuthStateChanged(user => {
      if(user){
        //logged in
        this.onLoad(this.tabsPage);
        this.login = true;
        console.log("logged in");
      }else{
        //not logged in
        this.onLoad(this.rootPage);
        this.login = false;
        console.log("not logged in");
      }
    });
    this.quoteData.initialize();
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  logout() {
    if(this.authService.logout()){
      console.log("logout success");
      this.favList.reset(); //reset local array setelah logout
    }else{
      console.log("logout fail");
    }
  }
  onLoad(page: any) {
    this.navCtrl.setRoot(page);
    this.menuCtrl.close();
  }
}

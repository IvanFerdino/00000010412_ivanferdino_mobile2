import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import { SetLocationPage } from '../set-location/set-location';
import { Loc } from '../../data/loc.class';
import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File, Entry, FileError } from '@ionic-native/file';

declare var cordova: any;
/**
 * Generated class for the AddPlacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-place',
  templateUrl: 'add-place.html',
})
export class AddPlacePage {
  marker: Loc;
  lat: number = null;
  lng: number= null;
  info: string =null;

  imageUrl:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, private geloLoc: Geolocation, private androidPermissions: AndroidPermissions, private toast: ToastController, private camera: Camera, private file: File) {
    this.marker = new Loc(this.lat,this.lng);
    this.requestPermission();
  }

  requestPermission() {
     //CEK PERMISSION
     let toast = this.toast.create({
      message: 'User was added successfully',
      duration: 3000,
      position: 'top'
    });
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
      success => {
        toast.setMessage("Camera Granted!");
        toast.present();
          
      }).catch(
        err => {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA);
          toast.setMessage("Camera Not Granted!");
          toast.present();
        }
    );

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
      success => {
        toast.setMessage("Fine Location Granted!");
        toast.present();
          
      }).catch(
        err => {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA);
          toast.setMessage("Fine Location Not Granted!");
          toast.present();
        }
    );
    //CEK PERMISSION SAMPE SINI
  }

  setMarker() {
    this.marker.setLatLng(this.lat, this.lng);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPlacePage');
  }

  onOpenMap(){
    let modal = this.modalCtrl.create(SetLocationPage);
    modal.present();
    modal.onDidDismiss(data => {
      if(data!=null){
        this.lat = data.lat;
        this.lng = data.lng;
        console.log(data);
        this.setMarker();
      }
    });
  }

  onLocate() {
    this.geloLoc.getCurrentPosition().then(
      currLocation => {
        console.log("a",currLocation);
        this.lat = currLocation.coords.latitude;
        this.lng = currLocation.coords.longitude;
        this.setMarker();
      }
    ).catch(
        error => {
          console.log(error);
          this.info = error;
      }
    );
  }

  onTakePhoto() {
    //CEK PERMISSIION, KALO BELOM REQUEST LG
    //PROSES AMBIL FOTO
    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
    }

    this.camera.getPicture(options).then(
      (imageData)=> {
        const currentName = imageData.replace(/^.*[\\\/]/, '');
        const path = imageData.replace(/[^\/]*$/, '');

        this.file.moveFile(path, currentName, cordova.file.dataDirectory, currentName).then(
          (data:Entry)=>{
            this.imageUrl = data.nativeURL;
            this.camera.cleanup();
            //File.removeFile(path, currentName); // alternative
          }).catch(
            (err:FileError)=>{
              this.imageUrl = "";
              this.camera.cleanup();
            }
        );

      },
      (err)=>{
        console.log(err);
      }
    );
    //AMBIL FOTO SAMPE SINI
  }
}
